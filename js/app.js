(function () {
    String.prototype.endsWith = function (suffix) {
        return this.indexOf(suffix, this.length - suffix.length) !== -1;
    };

    window.addEventListener("tizenhwkey", function (ev) {
        var activePopup = null,
            page = null,
            pageid = "";

        if (ev.keyName === "back") {
            activePopup = document.querySelector(".ui-popup-active");
            page = document.getElementsByClassName("ui-page-active")[0];
            pageid = page ? page.id : "";

            if (pageid === "main" && !activePopup) {
                try {
                    tizen.application.getCurrentApplication().exit();
                } catch (ignore) {
                }
            } else {
                window.history.back();
            }
        }
    });

    var fList, zList, jotterDir, editor, activeFileName, activeFile, doubleTapTimer = null;

    function openFileContents() {
        tizen.filesystem.resolve("documents/jotter/" + activeFileName,
            function (f) {
                activeFile = f;
                f.readAsText(
                    function (str) {
                        editor.value = str;
                    },
                    function (e) {
                        console.log("Error " + e.message);
                    },
                    "UTF-8");
                tau.openPopup('#file-editor-popup');
            },
            null,
            "rw");
    }

    function saveFileContents() {
        activeFile.openStream("w",
            function (fs) {
                fs.write(editor.value);
                fs.close();
            },
            function (error) {
                console.log("Error " + error.message);
            },
            "UTF-8");
    }

    function doubleTapHandler() {
        navigator.vibrate(50);
        if (doubleTapTimer == null) {
            doubleTapTimer = setTimeout(function () {
                // single tap
                doubleTapTimer = null;
                openFileContents();
            }, 500);
        } else {
            // double tap
            clearTimeout(doubleTapTimer);
            doubleTapTimer = null;
            tau.openPopup('#file-options-popup');
        }
    }

    function addToList(name) {
        var newLi = document.createElement('li');
        var newA = document.createElement('a');
        newA.href = '#';
        newA.innerText = name;
        newLi.appendChild(newA);
        newLi.id = name;
        newLi.addEventListener('click', function () {
            activeFileName = this.id;
            doubleTapHandler();
        });
        fList.appendChild(newLi);
    }

    function compareByModified(a, b) {
        if (a.modified > b.modified)
            return -1;
        if (a.modified < b.modified)
            return 1;
        return 0;
    }

    function refreshFiles() {
        function onsuccess(files) {
            if (zList != null) {
                zList.destroy()
            }
            fList.innerHTML = '';
            files.sort(compareByModified);
            for (var i = 0; i < files.length; i++) {
                addToList(files[i].name)
            }
            if (files.length > 0) {
                zList = new tau.widget.ArcListview(fList);
                zList.refresh();
            }
        }

        function onerror(error) {
            console.log(
                "The error " + error.message + " occurred when listing the files in the selected folder");
        }

        tizen.filesystem.resolve("documents/jotter",
            function (dir) {
                jotterDir = dir;
                jotterDir.listFiles(onsuccess, onerror);
            },
            function () {
                tizen.filesystem.resolve("documents",
                    function (dir) {
                        jotterDir = dir.createDirectory("jotter");
                        jotterDir.listFiles(onsuccess, onerror);
                    },
                    function (e) {
                        console.log("Error: " + e.message);
                    },
                    "rw");
            },
            "rw");
    }

    function createNewFile(name) {
        if (!name.endsWith('.txt')) {
            name = name + '.txt';
        }
        return jotterDir.createFile(name);
    }

    function renameFileName(oldname, newname) {
        if (!newname.endsWith('.txt')) {
            newname = newname + '.txt';
        }
        jotterDir.moveTo("documents/jotter/" + oldname, "documents/jotter/" + newname, false);
    }

    function init() {
        fList = document.getElementById('jotter-file-list');
        editor = document.getElementById('text-editor');
        var newJotter = document.getElementById('new-jotter');
        newJotter.addEventListener('click', function () {
            tau.openPopup('#new-file-popup');
        });
        var newFileNameSave = document.getElementById('new-file-name-save');
        newFileNameSave.addEventListener('click', function () {
            var newname = document.getElementById('new-file-name');
            var nf = createNewFile(newname.value);
            if (nf != null) {
                refreshFiles();
                activeFileName = nf.name;
                openFileContents();
            }
            newname.value = '';
            tau.closePopup();
        });
        var saveFile = document.getElementById('save-file');
        saveFile.addEventListener('click', function () {
            saveFileContents();
            tau.closePopup();
        });

        var fileRename = document.getElementById('file-rename');
        fileRename.addEventListener('click', function () {
            var renameFile = document.getElementById('rename-file');
            renameFile.value = activeFileName;
            tau.openPopup('#file-rename-popup');
        });

        var renameFileSave = document.getElementById('rename-file-save');
        renameFileSave.addEventListener('click', function () {
            var renameFile = document.getElementById('rename-file');
            renameFileName(activeFileName, renameFile.value);
            tau.closePopup();
            refreshFiles();
        });

        var fileDelete = document.getElementById('file-delete');
        fileDelete.addEventListener('click', function () {
            var deleteme = confirm("Are you sure you would like to delete " + activeFileName + "?");
            if (deleteme) {
                jotterDir.deleteFile("documents/jotter/" + activeFileName);
                tau.closePopup();
                refreshFiles();
            }
        });

        refreshFiles();
    }

    window.onload = init;
}());